import com.sinergise.geometry.*;

import javax.sound.sampled.Line;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class WKTReader {

	private static final String EMPTY = "EMPTY";
	private static final String EOF = "EOF";
	private static final String COMMA = ",";
	private static final String L_PARENT = "(";
	private static final String R_PARENT = ")";
	
	/**
	 * Transforms the input WKT-formatted String into Geometry object
	 */
	public Geometry read(String wktString) {

		StringReader reader = new StringReader(wktString);
		WKTStreamTokenizer tokenizer = new WKTStreamTokenizer(reader);
		tokenizer.wordChars('a', 'z');
		tokenizer.wordChars('A', 'Z');
		tokenizer.wordChars('0','9');
		tokenizer.wordChars('.', '.');
		tokenizer.whitespaceChars(' ', ' ');

		Geometry g = null;

		try {
			g = parseGeometryData(tokenizer);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return g;
	}

	private Geometry parseGeometryData(WKTStreamTokenizer tokenizer) throws IOException, ParseException
	{
		String geometryType = tokenizer.getNextString();

		switch (geometryType)
		{
			case "POINT":
				return parsePointData(tokenizer, true);
			case "LINESTRING":
				return parseLineStringData(tokenizer);
			case "POLYGON":
				return parsePolygonData(tokenizer);
			case "MULTIPOINT":
				return parseMultiPointData(tokenizer);
			case "MULTILINESTRING":
				return parseMultiLineString(tokenizer);
			case "MULTIPOLYGON":
				return parseMultiPolygon(tokenizer);
			case "GEOMETRYCOLLECTION":
				return parseGeometryCollectionData(tokenizer);
			default:
				return null;
		}
	}

	private Point parsePointData(WKTStreamTokenizer tokenizer, boolean checkParenthesis) throws IOException, ParseException
	{
		if(checkParenthesis)
		{
			if(!tokenizer.getNextString().equalsIgnoreCase(L_PARENT) && !tokenizer.getTokenString().equalsIgnoreCase(EMPTY))
				throw new ParseException("POINT: Expecting ( or EMPTY but got "+tokenizer.getTokenString(), -1);
		}

		if(tokenizer.getTokenString().equalsIgnoreCase(EMPTY))
			return new Point();

		double x = tokenizer.getNextNumber();
		double y = tokenizer.getNextNumber();

		if(checkParenthesis)
		{
			if(!tokenizer.getNextString().equalsIgnoreCase(R_PARENT))
				throw new ParseException("POINT: Expecting ) but got "+tokenizer.getTokenString(), -1);
		}

		return new Point(x,y);
	}

	private LineString parseLineStringData(WKTStreamTokenizer tokenizer) throws IOException, ParseException
	{
		if(!L_PARENT.equalsIgnoreCase(tokenizer.getNextString()) && !EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("LINESTRING: Expecting ( or EMPTY but got "+tokenizer.getTokenString(), -1);

		if(EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			return new LineString();

		ArrayList<Double> points = new ArrayList<>();

		while(!R_PARENT.equalsIgnoreCase(tokenizer.getTokenString()))
		{
			points.add(tokenizer.getNextNumber());
			points.add(tokenizer.getNextNumber());

			if(!COMMA.equalsIgnoreCase(tokenizer.getNextString()) && !R_PARENT.equalsIgnoreCase(tokenizer.getTokenString()))
				throw new ParseException("LINESTRING: Expecting ) or ',' but got "+tokenizer.getTokenString(), -1);
		}

		return new LineString(points.stream().mapToDouble(Double::doubleValue).toArray());
	}

	private Polygon parsePolygonData(WKTStreamTokenizer tokenizer) throws IOException, ParseException
	{
		if(!L_PARENT.equalsIgnoreCase(tokenizer.getNextString()) && !EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("POLYGON: Expecting ( or EMPTY but got "+tokenizer.getTokenString(), -1);

		if(EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			return new Polygon();

		LineString ls = parseLineStringData(tokenizer);
		List<LineString> holes = new ArrayList<>();

		while(COMMA.equalsIgnoreCase(tokenizer.getNextString()))
		{
			holes.add(parseLineStringData(tokenizer));
		}

		if(!R_PARENT.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("POLYGON: Expecting ) but got "+tokenizer.getTokenString(), -1);

		LineString[] holesArray = new LineString[holes.size()];
		for(int i = 0; i < holes.size(); i++)
			holesArray[i] = holes.get(i);

		return new Polygon(ls, holesArray);
	}

	private MultiPoint parseMultiPointData(WKTStreamTokenizer tokenizer) throws IOException, ParseException
	{
		if(!L_PARENT.equalsIgnoreCase(tokenizer.getNextString()) && !EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("MULTIPOINT: Expecting ( or EMPTY but got "+tokenizer.getTokenString(), -1);

		if(EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			return new MultiPoint();

		ArrayList<Point> points = new ArrayList<>();
		boolean parenthesisCheck = true;

		if(!L_PARENT.equalsIgnoreCase(tokenizer.getNextString()))
		{
			parenthesisCheck = false;
		}

		tokenizer.pushBack();

		points.add(parsePointData(tokenizer, parenthesisCheck));
		while(COMMA.equalsIgnoreCase(tokenizer.getNextString()))
		{
			points.add(parsePointData(tokenizer, parenthesisCheck));
		}

		if(!R_PARENT.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("MULTIPOINT: Expecting ) but got "+tokenizer.getTokenString(), -1);

		Point[] pointsArr = new Point[points.size()];
		for(int i = 0; i < points.size(); i++)
			pointsArr[i] = points.get(i);

		return new MultiPoint(pointsArr);

	}

	private MultiLineString parseMultiLineString(WKTStreamTokenizer tokenizer) throws IOException, ParseException
	{
		if(!L_PARENT.equalsIgnoreCase(tokenizer.getNextString()) && !EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("MULTILINE: Expecting ( or EMPTY but got "+tokenizer.getTokenString(), -1);

		if(EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			return new MultiLineString();

		ArrayList<LineString> lineStrings = new ArrayList<>();

		lineStrings.add(parseLineStringData(tokenizer));
		while(COMMA.equalsIgnoreCase(tokenizer.getNextString()))
		{
			lineStrings.add(parseLineStringData(tokenizer));
		}

		if(!R_PARENT.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("MULTILINE: Expecting ) but got "+tokenizer.getTokenString(), -1);

		LineString[] lineStringArr = new LineString[lineStrings.size()];
		for(int i = 0; i < lineStrings.size(); i++)
			lineStringArr[i] = lineStrings.get(i);

		return new MultiLineString(lineStringArr);
	}

	private MultiPolygon parseMultiPolygon(WKTStreamTokenizer tokenizer) throws IOException, ParseException
	{
		if(!L_PARENT.equalsIgnoreCase(tokenizer.getNextString()) && !EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("MULTIPOLYGON: Expecting ( or EMPTY but got "+tokenizer.getTokenString(), -1);

		if(EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			return new MultiPolygon();

		ArrayList<Polygon> polygons = new ArrayList<>();

		polygons.add(parsePolygonData(tokenizer));
		while(COMMA.equalsIgnoreCase(tokenizer.getNextString()))
		{
			polygons.add(parsePolygonData(tokenizer));
		}

		if(!R_PARENT.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("MULTIPOLYGON: Expecting ) but got "+tokenizer.getTokenString(), -1);

		Polygon[] polygonArr = new Polygon[polygons.size()];
		for(int i = 0; i < polygons.size(); i++)
			polygonArr[i] = polygons.get(i);

		return new MultiPolygon(polygonArr);
	}

	private GeometryCollection parseGeometryCollectionData(WKTStreamTokenizer tokenizer) throws IOException, ParseException
	{
		if(!L_PARENT.equalsIgnoreCase(tokenizer.getNextString()) && !EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("GEOMETRYCOLLECTION: Expecting ( or EMPTY but got "+tokenizer.getTokenString(), -1);

		if(EMPTY.equalsIgnoreCase(tokenizer.getTokenString()))
			return new GeometryCollection();

		ArrayList<Geometry> geometries = new ArrayList<>();

		geometries.add(parseGeometryData(tokenizer));
		while(COMMA.equalsIgnoreCase(tokenizer.getNextString()))
		{
			geometries.add(parseGeometryData(tokenizer));
		}

		if(!R_PARENT.equalsIgnoreCase(tokenizer.getTokenString()))
			throw new ParseException("MULTIPOLYGON: Expecting ) but got "+tokenizer.getTokenString(), -1);

		return new GeometryCollection(geometries);
	}
}
