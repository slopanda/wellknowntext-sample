public class Tester {

    /**
     * Simple testing class to validate implementation.
     */
    public static void main(String[] args) {
        WKTReader reader = new WKTReader();
        WKTWriter writer = new WKTWriter();
        System.out.println(writer.write(reader.read("GEOMETRYCOLLECTION (POINT (40 10)," +
                "LINESTRING (10 10, 20 20, 10 40)," +
                "POLYGON ((40 40, 20 45, 45 30, 40 40)))")));

    }

}
