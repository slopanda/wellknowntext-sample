import com.sinergise.geometry.*;

import javax.sound.sampled.Line;

public class WKTWriter {
	
	/**
	 * Transforms the input Geometry object into WKT-formatted String. e.g.
	 * <pre><code>
	 * new WKTWriter().write(new LineString(new double[]{30, 10, 10, 30, 40, 40}));
	 * //returns "LINESTRING (30 10, 10 30, 40 40)"
	 * </code></pre>
	 */
	public String write(Geometry geom) {
		String geometryClass = geom.getClass().getSimpleName();

		switch (geometryClass)
		{
			case "Point":
				return "POINT "+pointToString((Point) geom);
			case "LineString":
				return "LINESTRING "+lineStringToString((LineString) geom);
			case "Polygon":
				return "POLYGON "+polygonToString((Polygon) geom);
			case "MultiPoint":
				return "MULTIPOINT "+multiPointToString((MultiPoint) geom);
			case "MultiLineString":
				return "MULTILINESTRING "+multiLineStringToString((MultiLineString) geom);
			case "MultiPolygon":
				return "MULTIPOLYGON "+multiPolygonToString((MultiPolygon) geom);
			case "GeometryCollection":
				return "GEOMETRYCOLLECTION "+geometryCollectionToString((GeometryCollection) geom);
			default:
				return null;
		}
	}

	private String pointToString(Point p)
	{
		if(p.isEmpty())
			return "EMPTY";

		return String.format("(%d %d)",(int)p.getX(), (int)p.getY());
	}

	private String lineStringToString(LineString ls)
	{
		if(ls.isEmpty())
			return "EMPTY";

		StringBuilder sb = new StringBuilder();
		sb.append("(");

		int idx = 0;
		while(idx < ls.getNumCoords())
		{
			sb.append(String.format("%d %d", (int)ls.getX(idx), (int)ls.getY(idx++)));
			if(idx < ls.getNumCoords())
				sb.append(", ");
		}
		sb.append(")");

		return sb.toString();
	}

	private String polygonToString(Polygon polygon)
	{
		if(polygon.isEmpty())
			return "EMPTY";

		StringBuilder sb = new StringBuilder();
		sb.append("(");
		sb.append(lineStringToString(polygon.getOuter()));

		int holesProcessed = 0;

		if(holesProcessed < polygon.getNumHoles())
		{
			sb.append(", ");
			sb.append(lineStringToString(polygon.getHole(holesProcessed++)));
		}

		sb.append(")");
		return sb.toString();
	}

	private String multiPointToString(MultiPoint multiPoint)
	{
		if(multiPoint.isEmpty())
			return "EMPTY";

		StringBuilder sb = new StringBuilder();
		sb.append("(");

		int pointsProcessed = 0;
		while(pointsProcessed < multiPoint.size())
		{
			sb.append(pointToString(multiPoint.get(pointsProcessed++)));
			if(pointsProcessed < multiPoint.size())
				sb.append(", ");
		}

		sb.append(")");
		return sb.toString();
	}

	private String multiLineStringToString(MultiLineString multiLineString)
	{
		if(multiLineString.isEmpty())
			return "EMPTY";

		StringBuilder sb = new StringBuilder();
		sb.append("(");

		int linesProcessed = 0;
		while(linesProcessed < multiLineString.size())
		{
			sb.append(lineStringToString(multiLineString.get(linesProcessed++)));
			if(linesProcessed < multiLineString.size())
				sb.append(", ");
		}

		sb.append(")");
		return sb.toString();
	}

	private String multiPolygonToString(MultiPolygon multiPolygon)
	{
		if(multiPolygon.isEmpty())
			return "EMPTY";

		StringBuilder sb = new StringBuilder();
		sb.append("(");

		int polygonsProcessed = 0;
		while(polygonsProcessed < multiPolygon.size())
		{
			sb.append(polygonToString(multiPolygon.get(polygonsProcessed++)));
			if(polygonsProcessed < multiPolygon.size())
				sb.append(", ");
		}

		sb.append(")");
		return sb.toString();
	}

	private String geometryCollectionToString(GeometryCollection geometryCollection)
	{
		if(geometryCollection.isEmpty())
			return "EMPTY";

		StringBuilder sb = new StringBuilder();
		sb.append("(");

		int geomProcessed = 0;
		while(geomProcessed < geometryCollection.size())
		{
			sb.append(write(geometryCollection.get(geomProcessed++)));
			if(geomProcessed < geometryCollection.size())
				sb.append(", ");
		}

		sb.append(")");
		return sb.toString();
	}
}
