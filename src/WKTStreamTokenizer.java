import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;

/**
 * Extension of StreamTokenizer with specialization for WKT.
 */
public class WKTStreamTokenizer extends StreamTokenizer {

    public WKTStreamTokenizer(Reader r)
    {
        super(r);
    }

    public String getNextString() throws IOException
    {
        this.nextToken();
        return getTokenString();
    }

    public Double getNextNumber() throws IOException
    {
        this.nextToken();
        return getTokenNumber();
    }

    public boolean isTokenNumber()
    {
        return this.ttype == StreamTokenizer.TT_NUMBER;
    }

    public Double getTokenNumber()
    {
        if(isTokenNumber())
            return this.nval;
        else
            return null;
    }

    /**
     * Function returns string value of current token, which could be either word, number or
     * some of the predefined symbols.
     * @return String value of token or null if token not in predefined symbols
     */
    public String getTokenString()
    {
        switch (this.ttype)
        {
            case StreamTokenizer.TT_NUMBER:
                return String.valueOf(this.nval);
            case StreamTokenizer.TT_WORD:
                return this.sval;
            case '(':
                return "(";
            case ')':
                return ")";
            case ',':
                return ",";
            default:
                return null;
        }
    }
}
